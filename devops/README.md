# DUL ITS Deployment

## Details
```
Hostname: https://dul-huginn.cloud.duke.edu
```

## Shibboleth/Apache2 Proxy
Shibboleth (along with Apache2) is installed and configured to respond to `/user/*` route requests.  
  
Currently, valid users are able to authenticate and access the Huginn administrative login form.  
### Grouper Configuration
DevOps is currently working to restrict access by Grouper, using the following group:  
`urn:mace:duke.edu:groups:group-manager:roles:dul-huginn-admin`


